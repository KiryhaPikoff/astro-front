import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider as PaperProvider,} from 'react-native-paper';
import useCachedResources from './src/hooks/useCachedResources';
import Navigation from './src/navigations';
import GlobalProvider from "./src/context/Provider";

export default function App() {
    const isLoadingComplete = useCachedResources();

    if (!isLoadingComplete) {
        return null;
    } else {
        return (
            <GlobalProvider>
                <PaperProvider>
                    <SafeAreaProvider>
                        <Navigation />
                        <StatusBar />
                    </SafeAreaProvider>
                </PaperProvider>
            </GlobalProvider>
        );
    }
}
