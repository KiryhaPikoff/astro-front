import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Text, View} from '../../components/Other/Themed';
import Container from "../../components/common/Container";

export const About = () => {
  return (
      <Container>
        <View style={styles.container}>
          <Text style={styles.title}>Программу написал:</Text>
          <Text style={styles.description}>Студент гр. ПИбд-41</Text>
          <Text style={styles.description}>Пиков Кирилл</Text>
          <Text style={[styles.title, {marginTop: 40}]}>Вариант задания:</Text>
          <Text style={styles.description}>a. Наличие цифр, знаков препинания и занков арифметических операций</Text>
          <Text style={styles.description}>b. Алгоритм шифрования: СВС, без добавления случ. значения, алгоритм хеширования - SHA</Text>
          <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
        </View>
      </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 16,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
