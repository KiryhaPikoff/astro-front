import * as React from 'react';
import {useContext, useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {View} from '../../components/Other/Themed';
import {GlobalContext} from "../../context/Provider";
import {USERS_LIST} from "../../constants/fragments";
import UsersList from "../../components/common/UsersList";
import {BLOCKED, CREATE_USER, DISABLED, ENABLED} from "../../constants/actionsTypes";
import {update_account_enabled} from "../../context/actions/users/update_account_enabled";
import {update_password_policy} from "../../context/actions/users/update_password_policy";
import CustomButton from "../../components/common/CustomButton";
import Container from "../../components/common/Container";
import Message from "../../components/common/Message";
import {pw_policy_enable_all} from "../../context/actions/users/pw_policy_enable_all";
import {pw_policy_disable_all} from "../../context/actions/users/pw_policy_disable_all";
import {block_all} from "../../context/actions/users/block_all";
import {unblock_all} from "../../context/actions/users/unblock_all";
import {set_up_fragment} from "../../context/actions/stars/set_up_fragment";
import {change_password} from "../../context/actions/users/change_password";
import {register} from "../../context/actions/auth/register";
import Input from "../../components/common/Input";
import {refresh_all} from "../../context/actions/users/refresh_all";

export const Users = () => {

  const {
    usersDispatch,
    usersState: {data, fragment, loading, error, dataLoaded},
  } = useContext(GlobalContext)

  const {
    authDispatch
  } = useContext(GlobalContext)

  useEffect(() => {
    if (!dataLoaded) {
      refresh_all()(usersDispatch)
    }
  }, dataLoaded)

  const toggleModeByLogin = (login) => {
    return data
        .filter((user) => user.login === login)
        .map((user) => user.userAccountMode)
        .map(((mode) => mode === ENABLED ? BLOCKED : ENABLED));
  }

  const togglePwPolicyByLogin = (login) => {
    return data
        .filter((user) => user.login === login)
        .map((user) => user.passwordPolicy)
        .map(((mode) => mode === ENABLED ? DISABLED : ENABLED));
  }

  const onAccountEnabledChange = (login, value=null) => {
    update_account_enabled(
        login,
        value == null ? toggleModeByLogin(login) : value
    )(usersDispatch)
  }

  const onPasswordPolicyChange = (login, value=null) => {
    update_password_policy(
        login,
        value == null ? togglePwPolicyByLogin(login) : value
    )(usersDispatch)
  }

  const create_new_user = () => {
    set_up_fragment(CREATE_USER)(usersDispatch)
  }

  const [form, setForm] = useState({})
  const [errors, setErrors] = useState({})

  const onChange = ({name, value}) => {
    setForm({...form, [name]: value })

    if (value !== '') {
      setErrors(prev=>{
        return {...prev, [name]: null}
      });
    } else {
      setErrors(prev=>{
        return {...prev, [name]: "This field is required"}
      });
    }
  }

  const onSubmit = () => {
    if(!form.login || form.login.length === 0) {
      setErrors(prev=>{
        return {...prev, login: "Login must not be empty"}
      });
    }
    register({login: form.login, password: ""})(authDispatch)
    refresh_all()(usersDispatch)
  }

  return (
      <View style={styles.container}>
        <Container>
          {
            error !== null
                ? <Message
                    loading={loading}
                    danger
                    message={error.message}
                    onDismiss={() => {}}
                />
                : <></>
          }
        {
          fragment === USERS_LIST ?
              <>
                <CustomButton title={"Create new user"}
                              onPress={() => {create_new_user()}}
                              secondary
                />

                <CustomButton title={"Enable password policy for all"}
                              loading={loading}
                              disabled={loading}
                              onPress={() => {pw_policy_enable_all()(usersDispatch)}}
                              primary
                />
                <CustomButton title={"Disable password policy for all"}
                              loading={loading}
                              disabled={loading}
                              onPress={() => {pw_policy_disable_all()(usersDispatch)}}
                              primary
                />
                <CustomButton title={"Block all users"}
                              loading={loading}
                              disabled={loading}
                              onPress={() => {block_all()(usersDispatch)}}
                              primary
                />
                <CustomButton title={"Unblock all users"}
                              loading={loading}
                              disabled={loading}
                              onPress={() => {unblock_all()(usersDispatch)}}
                              primary
                />
                <UsersList
                    data={data}
                    onAccountEnabledChange={onAccountEnabledChange}
                    onPasswordPolicyChange={onPasswordPolicyChange}
                />
              </>
              : <></>
        }
        {
          fragment === CREATE_USER
              ? <>
                  {error !== null && error.message !== null
                      ? <Message
                          danger
                          message={error.message}
                      />
                      : <></>
                  }

                  <Input
                      label={"Login"}
                      placeholder={"Enter login"}
                      onChangeText={(value) => {
                        onChange({name: 'login', value});
                      }}
                      error={errors.username}
                  />

                  <CustomButton
                      primary
                      loading={loading}
                      disabled={loading}
                      title={"Submit"}
                      onPress={onSubmit}
                  />
              </>
              : <></>
        }
        </Container>
      </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 0,
    height: 1,
    width: '95%',
  },
});