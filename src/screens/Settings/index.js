import * as React from 'react';
import { StyleSheet } from 'react-native';

import EditScreenInfo from '../../components/Other/EditScreenInfo';
import { Text, View } from '../../components/Other/Themed';
import Container from "../../components/common/Container";
import Input, {ICON_POS_RIGHT} from "../../components/common/Input";
import {useCallback, useContext, useEffect, useState} from "react";
import {useNavigation} from "@react-navigation/core";
import {GlobalContext} from "../../context/Provider";
import {LOGIN} from "../../constants/RouteNames";
import {useFocusEffect} from "@react-navigation/native";
import {clearAuthState, register} from "../../context/actions/auth/register";
import CustomButton from "../../components/common/CustomButton";
import {change_password} from "../../context/actions/users/change_password";
import Message from "../../components/common/Message";

export const Settings = () => {

  const [form, setForm] = useState({})
  const [errors, setErrors] = useState({})
  const [successfull, setSuccessfull] = useState(null)

  const {
    authDispatch,
    authState: {error, loading},
  } = useContext(GlobalContext)

  const onChange = ({name, value}) => {
    setForm({...form, [name]: value })

    if (value !== '') {
      setErrors(prev=>{
        return {...prev, [name]: null}
      });
    } else {
      setErrors(prev=>{
        return {...prev, [name]: "This field is required"}
      });
    }
  }

  const onSubmit = () => {
    if (!form.oldPassword) {
      form.oldPassword = ''
    }
    if (!form.newPassword) {
      form.newPassword = ''
    }
    if (!form.passwordConfirm) {
      form.passwordConfirm = ''
    }
    if(form.newPassword !== form.passwordConfirm) {
      setErrors(prev=>{
        return {...prev, passwordConfirm: "Passwords don't matches"}
      });
    }
    change_password(form)(authDispatch)
    setSuccessfull(true)
  }

  return (
      <Container>
          <Text style={styles.title}>Change password</Text>

          {successfull !== null && successfull
              ? <Message
                  success
                  message={"Password successfully changed"}
                  onDismiss={() => {}}
              />
              : <></>
          }

          {error !== null
              ? <Message
                  danger
                  message={error.message}
                  onDismiss={() => {}}
              />
              : <></>
          }

        <Input
            label={"Old password:"}
            placeholder={"Enter your old password here"}
            secureTextEntry={true}
            icon={<Text>Show</Text>}
            iconPosition={ICON_POS_RIGHT}
            error={errors.password}
            onChangeText={(value) => {
              onChange({name: 'oldPassword', value});
            }}
        />

        <Input
            label={"New password:"}
            placeholder={"Enter your new password here"}
            secureTextEntry={true}
            icon={<Text>Show</Text>}
            iconPosition={ICON_POS_RIGHT}
            error={errors.password}
            onChangeText={(value) => {
              onChange({name: 'newPassword', value});
            }}
        />

        <Input
            label={"Confirm new password:"}
            placeholder={"Enter your new password again"}
            secureTextEntry={true}
            icon={<Text>Show</Text>}
            iconPosition={ICON_POS_RIGHT}
            error={errors.passwordConfirm}
            onChangeText={(value) => {
              onChange({name: 'passwordConfirm', value});
            }}
        />

        <CustomButton
            primary
            loading={loading}
            disabled={loading}
            title={"Submit"}
            onPress={onSubmit}
        />

      </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 16,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
