import * as React from 'react';
import {useCallback, useContext, useEffect, useState} from 'react';
import Container from "../../components/common/Container";
import RegisterComponent from "../../components/Register";
import {clearAuthState, register} from "../../context/actions/auth/register";
import {GlobalContext} from "../../context/Provider";
import {LOGIN} from "../../constants/RouteNames";
import {useNavigation} from "@react-navigation/core";
import {useFocusEffect} from '@react-navigation/native'

export const Register = () => {
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const {navigate} = useNavigation()
    const {
        authDispatch,
        authState: {error, loading, data},
    } = useContext(GlobalContext)

    useEffect(() => {
        if (data) {
            navigate(LOGIN)
        }
    }, [data])

    useFocusEffect(
        useCallback(() => {
            if (data || error) {
                clearAuthState()(authDispatch);
            }
        }, [data, error])
    )

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value })

        if (value !== '') {
            setErrors(prev=>{
                return {...prev, [name]: null}
            });
        } else {
            setErrors(prev=>{
                return {...prev, [name]: "This field is required"}
            });
        }
    }

    const onSubmit = () => {
        if(!form.login) {
            setErrors(prev=>{
                return {...prev, login: "Please add a username"}
            });
        }
        if(!form.password) {
            setErrors(prev=>{
                return {...prev, password: "Please enter a password"}
            });
        }
        if(form.password !== form.passwordConfirm) {
            setErrors(prev=>{
                return {...prev, passwordConfirm: "Passwords don't matches"}
            });
        }
        if (
            Object.values(form).length === 3 &&
            Object.values(form).every((item) => item.trim().length > 0) &&
            Object.values(errors).every((item) => !item)
        ) {
            register(form)(authDispatch)
        }
    }

  return (
      <Container>
        <RegisterComponent
            onSubmit={onSubmit}
            onChange={onChange}
            form={form}
            errors={errors}
            error={error}
            loading={loading}
        />
      </Container>
  );
}
