import * as React from 'react';
import {useContext, useEffect, useState} from 'react';
import LoginComponent from "../../components/Login";
import {useNavigation} from "@react-navigation/core";
import {GlobalContext} from "../../context/Provider";
import {HOME_NAVIGATOR} from "../../constants/RouteNames";
import {login} from "../../context/actions/auth/login";
import Container from "../../components/common/Container";
import RNExitApp from 'react-native-exit-app';

export const Login = () => {
  const [form, setForm] = useState({})
  const [errors, setErrors] = useState({})
  const {navigate} = useNavigation()

  const {
    authDispatch,
    authState: {error, loading, data, isLoggedIn, loginMaxAttempts},
  } = useContext(GlobalContext)

  useEffect(() => {
    if (isLoggedIn) {
      navigate(HOME_NAVIGATOR)
    }
  }, [data])

  useEffect(() => {
    if (loginMaxAttempts === 0) {
      RNExitApp.exitApp();
    }
  }, [loginMaxAttempts])

  const onChange = ({name, value}) => {
      setForm({...form, [name]: value })
  }

  const onSubmit = () => {
    if(!form.login) {
      setErrors(prev=>{
        return {...prev, login: "Please enter a login"}
      });
    }
    if (
        form.login
    ) {
      if (!form.password) {
        form.password = ''
      }
      login(form)(authDispatch)
    }
  }

  return (
      <Container>
        <LoginComponent
            onSubmit={onSubmit}
            onChange={onChange}
            form={form}
            errors={errors}
            error={error}
            loading={loading}
        />
      </Container>
  );
}
