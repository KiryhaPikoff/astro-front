import * as React from 'react';
import {useContext, useEffect, useState} from 'react';
import {StyleSheet, View,} from 'react-native';
import StarsList from "../../components/common/StarsList";
import {GlobalContext} from "../../context/Provider";
import {refresh_stars} from "../../context/actions/stars/refresh_stars";
import {STARS_LIST, UPDATE_STAR} from "../../constants/actionsTypes";
import {UpdateStar} from "../../components/common/UpdateStar";
import {set_up_fragment} from "../../context/actions/stars/set_up_fragment";

export const Stars = () => {

    const {
        starsDispatch,
        starsState: {data, fragment, dataLoaded},
    } = useContext(GlobalContext)

    const [selectedStar, setSelectedStar] = useState(null);

    useEffect(() => {
            if (!dataLoaded) {
                refresh_stars()(starsDispatch)
            }
        }, [dataLoaded]);

    useEffect(() => {
        if (selectedStar !== null) {
            set_up_fragment(UPDATE_STAR)(starsDispatch)
        }
    }, [selectedStar])

    const onSubmit = (form) => {
        update(form)
        setSelectedStar(null)
        set_up_fragment(STARS_LIST)(starsDispatch)
    }

    const onBack = () => {
        setSelectedStar(null)
        set_up_fragment(STARS_LIST)(starsDispatch)
    }

    return (
        <View style={styles.container}>
            {fragment === STARS_LIST
                ? <StarsList
                    data={data}
                    setSelectedStar={setSelectedStar}
                />
                : <></>
            }
            {fragment === UPDATE_STAR
                ? <UpdateStar
                    star={selectedStar}
                    onSubmit={onSubmit}
                    onBack={onBack}
                  />
                : <></>
            }
        </View>
      );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 0,
        height: 1,
        width: '95%',
    },
});

