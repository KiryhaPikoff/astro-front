export const tintColorLight = '#2f95dc';
export const tintColorDark = '#fff';
export const danger = '#ff9191';
export const loading = '#253bb2';

export default {
  light: {
    text: '#8e3636',
    background: '#6e95a3',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#6aa6e6',
    background: '#517fa0',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },
};
