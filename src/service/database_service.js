import axiosInstance from "../helpers/dbasx.js";
import {SET_UP_ALL} from "../context/reducers/stars";

export async function selectAll(dispatch) {
    return axiosInstance.get("/stars")
        .then( (result) => {
            dispatch({
                type: SET_UP_ALL,
                payload: {data: result.data}
            })
        })
        .catch((err) => {
            console.log(err)
        })
}

export async function insertSql( {id, name, isSuperStar} ) {
    return axiosInstance.post("/stars", {id: id, name: name, isSuperStar: isSuperStar})
        .catch((err) => {
            console.log(err)
        })
}

export async function updateSql( {id, name, isSuperStar} ) {
    return axiosInstance.patch("/stars/" + id + "/", {id: id, name: name, isSuperStar: isSuperStar})
        .catch((err) => {
            console.log(err)
        })
}

export async function deleteSql(id) {
    return axiosInstance.delete("/stars/" + id)
        .catch((err) => {
            console.log(err)
        })
}


