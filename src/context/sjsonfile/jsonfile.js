let RNFS = require('react-native-fs');
let path = RNFS.DocumentDirectoryPath + '/json_format.json';

export async function getAll() {
    return JSON.parse(RNFS.readFile(path)
        .catch((err) => {
            console.log(err.message);
        }));
}

export async function createRow( {id, name, isSuperStar} ) {
    let jsonData = JSON.parse(RNFS.readFile(path)
        .catch((err) => {
            console.log(err.message);
    }));
    jsonData = {
        ...jsonData,
        jsonData: {
            id, name, isSuperStar
        }
    }
    await RNFS.writeFile(path, jsonData)
}

export async function updateRow( {id, name, isSuperStar} ) {
    let jsonData = JSON.parse(RNFS.readFile(path)
        .catch((err) => {
            console.log(err.message);
   }));
    jsonData = {
        ...jsonData,
        jsonData: jsonData.map(star => {
            if (star.id !== id) {
                return star
            }
            return {
                ...star,
                name: name,
                isSuperStar: isSuperStar,
            }
        })
    }
    await RNFS.writeFile(path, jsonData)
}

export async function deleteRow( {id, name, isSuperStar} ) {
    let jsonData = JSON.parse(RNFS.readFile(path)
        .catch((err) => {
            console.log(err.message);
        }));
    jsonData = {
        ...jsonData,
        jsonData: jsonData.filter(star => {
            if (star.id === id) {
                return star
            }
        })
    }
    await RNFS.writeFile(path, jsonData)
}