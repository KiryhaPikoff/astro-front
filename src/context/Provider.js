import {createContext, useReducer} from "react";
import authInitialState from "./initialsStates/auth";
import starsInitialState from "./initialsStates/stars";
import usersInitialState from "./initialsStates/users";
import {auth} from "./reducers/auth";
import {stars} from "./reducers/stars";
import {users} from "./reducers/users";
import React from 'react'

export const GlobalContext = createContext({})

const GlobalProvider = ({children}) => {
    const [authState, authDispatch] = useReducer(auth, authInitialState,);
    const [starsState, starsDispatch] = useReducer(stars, starsInitialState,);
    const [usersState, usersDispatch] = useReducer(users, usersInitialState,);

    return (
        <GlobalContext.Provider value={{
            authState, authDispatch,
            starsState, starsDispatch,
            usersState, usersDispatch
        }}>
            {children}
        </GlobalContext.Provider>
    )
}

export default GlobalProvider;
