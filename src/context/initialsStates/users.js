import {USERS_LIST} from "../../constants/fragments";
import {DISABLED, ENABLED} from "../../constants/actionsTypes";

export default {
    data: [
        {
            login: "kiryha",
            role: "admin",
            passwordPolicy: DISABLED,
            userAccountMode: ENABLED
        },
        {
            login: "vasya",
            role: "admin",
            passwordPolicy: DISABLED,
            userAccountMode: ENABLED
        },
        {
            login: "sdolld",
            role: "admin",
            passwordPolicy: DISABLED,
            userAccountMode: ENABLED
        },
        {
            login: "opasdw",
            role: "admin",
            passwordPolicy: DISABLED,
            userAccountMode: ENABLED
        }
    ],
    dataLoaded: false,
    error: null,
    loading: false,
    fragment: USERS_LIST
}
