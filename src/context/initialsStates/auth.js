export default {
    isLoggedIn: true,
    username: 'is_not_logged_in',
    role: 'admin',
    data: null,
    error: null,
    loading: false,
    loginMaxAttempts: 3
}
