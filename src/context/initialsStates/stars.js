import {STARS_LIST} from "../../constants/actionsTypes";

export default {
    data: [
        {
            id: "3ee2e736-7f4d-4478-9869-b1ff18f13b5b",
            name: "Alokfadsa",
            absoluteTemperature: 12000.3,
            relativeLuminosity: 3.282,
            relativeRadius: 6.3215,
            absoluteMagnitude: 214.0,
            starType: "Red Dwarf",
            starColor: "red",
            spectralClass: "B"
        },
        {
            id: "22e2e736-7f4d-4478-9869-b1ff18f13b5b",
            name: "Lopadga",
            absoluteTemperature: 3300.245,
            relativeLuminosity: 23.282,
            relativeRadius: 600.3215,
            absoluteMagnitude: 2014.0,
            starType: "Blue Dwarf",
            starColor: "blue",
            spectralClass: "G"
        },
        {
            id: "32e2e736-7f4d-4478-9869-b1ff18f13b5b",
            name: "Gdaeep",
            absoluteTemperature: 3300.245,
            relativeLuminosity: 23.282,
            relativeRadius: 600.3215,
            absoluteMagnitude: 2014.0,
            starType: "Blue Dwarf",
            starColor: "blue",
            spectralClass: "M"
        }
    ],
    dataLoaded: false,
    error: null,
    loading: false,
    fragment: STARS_LIST
}
