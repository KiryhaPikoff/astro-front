import {SQLite} from 'react-native-sqlite'

export async function onServiceUp() {
    return SQLite.openDatabase('rndtbs.sqlite', function (error, connection) {
        if (error) {
            console.log('Failed to open database:', error);
            return;
        }
        connection.executeSql(`
            create table if not exists stars (
            (
                id bigint not null primary key,
                name varchar not null,
                is_super_star boolean not null
            );
        `);
    });
}

export async function selectAll() {
    return SQLite.openDatabase('rndtbs.sqlite', function (error, connection) {
        if (error) {
            console.log('Failed to open database:', error);
            return [];
        }
        let stars = connection.executeSql('SELECT * FROM stars');
        return stars
    });
}

export async function insertSql( {name, isSuperStar} ) {
    return SQLite.openDatabase('rndtbs.sqlite', function (error, connection) {
        if (error) {
            console.log('Failed to open database:', error);
            return;
        }
        connection.executeSql(
            'insert into stars values (seq.next_val(), name_param, is_super_star_param)',
            {
                'name_param': name,
                'is_super_star_param': isSuperStar
            }
        );
    });
}

export async function updateSql( {id, name, isSuperStar} ) {
    return SQLite.openDatabase('rndtbs.sqlite', function (error, connection) {
        if (error) {
            console.log('Failed to open database:', error);
            return;
        }
        connection.executeSql(
            'update stars set name=$name_param, is_super_star=$is_super_star_param where id=$id_param',
            {
                'id_param': id,
                'name_param': name,
                'is_super_star_param': isSuperStar
            }
        );
    });
}

export async function deleteSql(id) {
    return SQLite.openDatabase('rndtbs.sqlite', function (error, connection) {
        if (error) {
            console.log('Failed to open database:', error);
            return;
        }
        connection.executeSql(
            'delete from stars where id=$id_param',
            {
                'id_param': id,
            }
        );
    });
}


