import {CLEAR_ERRORS, LOADING, LOADING_FAIL, LOADING_SUCCESS} from "../../../constants/actionsTypes";
import axiosInstance from "../../../helpers/axiosIntercepter";

export const change_password = (request) => dispatch => {
    dispatch({
        type: LOADING
    })
    axiosInstance.post("/users/v1/change-password", request)
        .then( (_) => {
            dispatch({
                type: CLEAR_ERRORS,
            })
            dispatch({
                type: LOADING_SUCCESS,
            })
        })
        .catch((err) => {
            dispatch({
                type: LOADING_FAIL,
                payload: err.response
                    ? err.response.data
                    : {message: "Something went wrong, try again"}
            })
        })
}
