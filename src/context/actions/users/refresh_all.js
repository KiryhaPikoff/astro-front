import {
    LOADING, LOADING_FAIL,
    LOADING_SUCCESS,
    REGISTER_FAIL,
    REGISTER_LOADING,
    REGISTER_SUCCESS
} from "../../../constants/actionsTypes";
import axiosInstance from "../../../helpers/axiosIntercepter";
import {set_up_fragment} from "../stars/set_up_fragment";
import {USERS_LIST} from "../../../constants/fragments";

export const refresh_all = () => dispatch => {
    dispatch({
        type: LOADING
    })
    axiosInstance.get("/users/v1/")
        .then( (res) => {
            dispatch({
                type: LOADING_SUCCESS,
                payload: res.data
            })
        })
        .catch((err) => {
            dispatch({
                type: LOADING_FAIL,
                payload: err.response
                    ? err.response.data
                    : {message: "Something went wrong, try again"}
            })
        })
    set_up_fragment(USERS_LIST)(dispatch)
}
