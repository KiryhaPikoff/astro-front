import {LOADING, LOADING_FAIL} from "../../../constants/actionsTypes";
import axiosInstance from "../../../helpers/axiosIntercepter";
import {refresh_all} from "./refresh_all";

export const update_account_enabled = (login, value) => dispatch => {
    dispatch({
        type: LOADING
    })
    axiosInstance.post("/users/v1/" + login, {userAccountMode: value[0]})
        .then( (_) => {
            refresh_all()(dispatch)
        })
        .catch((err) => {
            dispatch({
                type: LOADING_FAIL,
                payload: err.response
                    ? err.response.data
                    : {message: "Something went wrong, try again"}
            })
        })
}
