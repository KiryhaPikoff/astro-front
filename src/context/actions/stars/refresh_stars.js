import {FILE, REGISTER_SUCCESS, SQL, STARS_LIST} from "../../../constants/actionsTypes";
import {selectAll} from "../../../service/database_service.js"
import {set_up_fragment} from "./set_up_fragment";
import {SET_UP_ALL} from "../../reducers/stars";


export const refresh_stars = () => async dispatch => {
    await selectAll(dispatch)
    set_up_fragment(dispatch, STARS_LIST)
}