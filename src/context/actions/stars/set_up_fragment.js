import {SET_UP_FRAGMENT} from "../../reducers/stars";

export const set_up_fragment = (fragment) => dispatch => {
    dispatch({
        type: SET_UP_FRAGMENT,
        payload: {fragment: fragment}
    })
}