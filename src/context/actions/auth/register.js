import axiosInstance from "../../../helpers/axiosIntercepter";
import {CLEAR_AUTH_STATE, REGISTER_FAIL, REGISTER_LOADING, REGISTER_SUCCESS} from "../../../constants/actionsTypes";

export const clearAuthState = () => dispatch => {
    dispatch({
        type: CLEAR_AUTH_STATE,
    })
}

export const register = ({
    login,
    password
}) => dispatch => {
    dispatch({
        type: REGISTER_LOADING
    })
    axiosInstance.post("/auth/v1/register", {login, password})
    .then( (res) => {
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        })
    })
    .catch((err) => {
        dispatch({
            type: REGISTER_FAIL,
            payload: err.response
                ? err.response.data
                : {error: "Something went wrong, try again"}
        })
    })
}