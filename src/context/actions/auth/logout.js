import {LOGOUT} from "../../../constants/actionsTypes";

export const logout = () => dispatch => {
    dispatch({
        type: LOGOUT
    })
}