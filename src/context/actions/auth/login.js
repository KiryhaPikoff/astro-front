import axiosInstance from "../../../helpers/axiosIntercepter";
import {LOGIN_FAIL, LOGIN_LOADING, LOGIN_SUCCESS} from "../../../constants/actionsTypes";
import {AsyncStorage} from "react-native";

export const login = ({
    login,
    password
}) => dispatch => {
    dispatch({
        type: LOGIN_LOADING
    })
    axiosInstance.post("/auth/v1/tokens", {login, password})
    .then( (res) => {
        console.log(res)
        console.log(res.data)
        console.log(res.data.access)
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        })
        AsyncStorage.setItem(
            'token',
            res.data.access
        );
    })
    .catch((err) => {
        console.log(err)
        console.log(err.data)
        console.log(err.response)
        console.log(err.response.data)
        console.log(err.response.data.message)
        dispatch({
            type: LOGIN_FAIL,
            payload: err.response
                ? err.response.data
                : {error: "Something went wrong, try again"}
        })
    })
}