import {
    LOADING, LOADING_FAIL, LOADING_SUCCESS,
    REGISTER_FAIL,
    REGISTER_LOADING,
    REGISTER_SUCCESS,
    UPDATE_ACCOUNT_MODE
} from "../../constants/actionsTypes";
import {SET_UP_ALL, SET_UP_FRAGMENT} from "./stars";

export const users = (state, {type, payload}) => {

    switch (type) {

        case LOADING:
            return {
                ...state,
                loading: true
            };

        case LOADING_SUCCESS:
            return {
                ...state,
                loading: false,
                data: payload,
            };

        case LOADING_FAIL:
            return {
                ...state,
                loading: false,
                error: payload,
            };


        case SET_UP_FRAGMENT:
            return {
                ...state,
                fragment: payload.fragment
            }

        default:
            return state;
    }
}