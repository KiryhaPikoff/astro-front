export const TOGGLE = "toggle";
export const TOGGLE_ALL = "toggleAll";
export const UN_TOGGLE_ALL = "unToggleAll";
export const ADD_STAR = "addStar";
export const SET_UP_ALL = "setUpAll"
export const UPDATE_STAR = "updateStar"
export const CHANGE_DATA_SOURCE_TYPE = "CHANGE_DATA_SOURCE_TYPE"
export const SET_UP_FRAGMENT = "SET_UP_FRAGMENT"
export const DELETE_STAR = "DELETE_STAR"


function nextStarId(data) {
    const maxId = data.reduce((maxId, star) => Math.max(maxId, star.id), -1)
    return maxId + 1
}

export const stars = (state, {type, payload}) => {

    switch (type) {

        case DELETE_STAR:
            return {
                ...state,
                data: state.data.filter(star => star.id !== payload.id)
            };

        case CHANGE_DATA_SOURCE_TYPE:
            return {
                ...state,
                dataSourceType: payload.dataSourceType
            }

        case SET_UP_FRAGMENT:
            return {
                ...state,
                fragment: payload.fragment
            }

        case SET_UP_ALL:
            return {
                ...state,
                data: state.data !== payload.data ? payload.data : state.data
            }

        case ADD_STAR:
            return {
                ...state,
                data: [
                    ...data,
                    {
                        id: nextStarId(state.data),
                        name: payload.name,
                        isSuperStar: payload.isSuperStar,
                        toggled: false
                    }
                ]
            }

        case UPDATE_STAR:
            return {
                ...state,
                data: state.data.map(star => {
                    if (star.id !== payload.id) {
                        return star
                    }

                    return {
                        ...star,
                        name: payload.name,
                        isSuperStar: payload.isSuperStar,
                        toggled: false
                    }
                })
            }

        case TOGGLE:
            return {
                ...state,
                data: state.data.map(star => {
                    if (star.id !== payload.id) {
                        return star
                    }

                    return {
                        ...star,
                        toggled: star.toggled == null ? true : !star.toggled
                    }
                })
            };

        case TOGGLE_ALL:
            return {
                ...state,
                data: state.data.map(star => {
                    if (star.id !== payload) {
                        return {
                            ...star,
                            toggled: true
                        }
                    }
                })
            };

        case UN_TOGGLE_ALL:
            return {
                ...state,
                data: state.data.map(star => {
                    if (star.id !== payload) {
                        return {
                            ...star,
                            toggled: false
                        }
                    }
                })
            };
        default:
            return state;
    }
}