import {
    REGISTER_FAIL,
    REGISTER_LOADING,
    REGISTER_SUCCESS,
    CLEAR_AUTH_STATE,
    LOGIN_LOADING, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, CLEAR_ERRORS, LOADING_FAIL, LOADING, LOADING_SUCCESS
} from "../../constants/actionsTypes";
import jwt_decode from "jwt-decode";

export const auth = (state, {type, payload}) => {

    switch (type) {
        case REGISTER_LOADING:
            return {
                ...state,
                loading: true
            };

        case REGISTER_SUCCESS:
            return {
                ...state,
                loading: false,
                data: payload,
            };

        case REGISTER_FAIL:
            return {
                ...state,
                loading: false,
                error: payload,
            };

        case CLEAR_AUTH_STATE:
            return {
                ...state,
                loading: false,
                data: null,
            }

        case CLEAR_ERRORS:
            return {
                ...state,
                error: null,
            }

        case LOGIN_LOADING:
            return {
                ...state,
                loading: true,
            }

        case LOGIN_SUCCESS: {
            return {
                ...state,
                role: jwt_decode(payload.access).role,
                username: jwt_decode(payload.access).sub,
                loading: false,
                data: payload.access,
                isLoggedIn: true
            }
        }

        case LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                error: payload,
                loginMaxAttempts: state.loginMaxAttempts - 1
            };

        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false
            };

        case LOADING:
            return {
                ...state,
                loading: true
            };

        case LOADING_SUCCESS:
            return {
                ...state,
                loading: false,
                data: payload,
            };

        case LOADING_FAIL:
            return {
                ...state,
                loading: false,
                error: payload,
            };

        default:
            return state;
    }
}