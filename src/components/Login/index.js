import React from 'react';

import {Image, Text, TouchableOpacity, View} from 'react-native';
import Container from "../common/Container";
import Input, {ICON_POS_RIGHT} from "../common/Input";
import CustomButton from "../common/CustomButton";
import styles from "./styles"
import {useNavigation} from "@react-navigation/core";
import {REGISTER} from "../../constants/RouteNames";
import Message from "../common/Message";

const LoginComponent = ({
    onSubmit,
    onChange,
    form,
    errors,
    error,
    loading
}) => {
    const {navigate} = useNavigation()
    let logo = require('../../assets/images/logo.png')

    return (
        <Container>
            <Image source={logo} style={[styles.logoImage]} />

            <View style={styles.form}>

                {error !== null
                    ? <Message retry
                             onDismiss={() => {}}
                             danger message={error?.message}
                      />
                    : <></>
                }

                <Text style={styles.title}>Welcome to AstroApp</Text>
                <Text style={styles.subTitle}>Please login here</Text>

                <Input
                    label={"Login"}
                    placeholder={"Enter login"}
                    onChangeText={(value) => {
                        onChange({name: 'login', value});
                    }}
                    error={errors?.login}
                />

                <Input
                    label={"Password"}
                    placeholder={"Enter Password"}
                    secureTextEntry={true}
                    icon={<Text>Show</Text>}
                    iconPosition={ICON_POS_RIGHT}
                    error={errors?.password}
                    onChangeText={(value) => {
                        onChange({name: 'password', value});
                    }}
                />

                <CustomButton
                    primary
                    loading={loading}
                    disabled={loading}
                    title={"Submit"}
                    onPress={onSubmit}
                />

                <View style={styles.registerSection}>
                    <Text style={styles.infoText}>Need a new account?</Text>
                    <TouchableOpacity onPress={() => navigate(REGISTER)}>
                        <Text style={styles.registerLinkBtn}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </Container>
    );
};

export default LoginComponent;
