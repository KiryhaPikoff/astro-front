import { StyleSheet } from 'react-native';
import Colors from "../../assets/theme/Colors";

export default StyleSheet.create({
    logoImage: {
        height: 140,
        width: 140,
        alignSelf:'center',
        marginTop: 10,
        marginBottom: 10,
    },

    title: {
        fontSize: 21,
        textAlign: 'center',
        paddingTop: 20,
        fontWeight: "500",
    },

    subTitle: {
        fontSize: 17,
        textAlign: 'center',
        paddingVertical: 20,
        fontWeight: "500",
    },

    form: {
        paddingTop: 20,
    },

    registerSection: {
        flexDirection: 'row',
    },

    registerLinkBtn: {
        paddingLeft: 17,
        color: Colors.light.tabIconSelected,
        fontSize: 16
    },

    infoText: {
        fontSize: 17
    },
});