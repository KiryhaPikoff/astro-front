import React from 'react';

import {Image, Text, TouchableOpacity, View} from 'react-native';
import Container from "../common/Container";
import Input, {ICON_POS_RIGHT} from "../common/Input";
import CustomButton from "../common/CustomButton";
import styles from "./styles"
import {useNavigation} from "@react-navigation/core";
import {LOGIN, REGISTER} from "../../constants/RouteNames";
import Message from "../common/Message";

const RegisterComponent = ({
    onSubmit,
    onChange,
    form,
    errors,
    error,
    loading
}) => {
    const {navigate} = useNavigation()
    console.log(onChange)

    return (
        <Container>
            <View style={styles.form}>
                <Text style={styles.title}>Welcome to AstroApp</Text>
                <Text style={styles.subTitle}>Create a free account</Text>

                {error != null && <Message onDismiss={() => {}} danger message={error?.message} />}

                <Input
                    label={"Login"}
                    placeholder={"Enter login"}
                    onChangeText={(value) => {
                        onChange({name: 'login', value});
                    }}
                    error={errors.username}
                />

                <Input
                    label={"Password"}
                    placeholder={"Enter Password"}
                    secureTextEntry={true}
                    icon={<Text>Show</Text>}
                    iconPosition={ICON_POS_RIGHT}
                    error={errors.password}
                    onChangeText={(value) => {
                        onChange({name: 'password', value});
                    }}
                />

                <Input
                    label={"Confirm password"}
                    placeholder={"Enter Password again"}
                    secureTextEntry={true}
                    icon={<Text>Show</Text>}
                    iconPosition={ICON_POS_RIGHT}
                    error={errors.passwordConfirm}
                    onChangeText={(value) => {
                        onChange({name: 'passwordConfirm', value});
                    }}
                />

                <CustomButton
                    primary
                    loading={loading}
                    disabled={loading}
                    title={"Submit"}
                    onPress={onSubmit}
                />

                <View style={styles.registerSection}>
                    <Text style={styles.infoText}>Already registered?</Text>
                    <TouchableOpacity onPress={() => navigate(LOGIN)}>
                        <Text style={styles.registerLinkBtn}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Container>
    );
};

export default RegisterComponent;
