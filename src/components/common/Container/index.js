import React from 'react';
import styles from './styles'
import {ScrollView, View} from 'react-native';
import {useTheme} from "react-native-paper";

const Container = ({style, children}) => {
    const theme = useTheme();

    return (
        <ScrollView>
            <View style={[
                    styles.wrapper,
                    style,
                    {backgroundColor: theme.colors.background}
                ]}
            >{children}</View>
        </ScrollView>
    );
};

export default Container;
