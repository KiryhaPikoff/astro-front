import React, {useState} from 'react';

import {ActivityIndicator, Text, TextInput, TouchableOpacity, View} from 'react-native';
import styles from './styles'
import Colors from '../../../assets/theme/Colors'
import {danger as DANGER} from '../../../assets/theme/Colors'
import {loading as LOADING} from '../../../assets/theme/Colors'

export const ICON_POS_LEFT = 'left'
export const ICON_POS_RIGHT= 'right'

const CustomButton = ({
   title,
    disabled,
    loading,
    primary,
    secondary,
    danger,
    onPress,
}) => {
    const getBackgroundColor = () => {
        if (primary) {
            return Colors.light.tabIconDefault
        }
        if (secondary) {
            return Colors.light.tint
        }
        if (danger) {
            return DANGER
        }
    }

    return (

        <TouchableOpacity
            style={[styles.wrapper, {backgroundColor: getBackgroundColor()}]}
            disabled={disabled}
            onPress={onPress}
        >
            <View style={[styles.loaderSection]}>
                {loading && <ActivityIndicator color={LOADING} />}
                {!loading && title &&
                    <Text
                        style={{
                            color: disabled ? 'black' : 'blue',
                            paddingLeft: loading ? 5 : 0
                        }}
                    >{title}</Text>}
            </View>
        </TouchableOpacity>
    );
};

export default CustomButton;
