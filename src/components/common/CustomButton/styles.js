import {StyleSheet} from 'react-native';
import Colors, {danger} from '../../../assets/theme/Colors'

export default StyleSheet.create({
    wrapper: {
        height: 42,
        paddingHorizontal: 5,
        marginVertical: 15,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },

    icon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 6,
    },

    loaderSection: {
        flexDirection: 'row',
    },

    textInput: {
        flex: 1,
    },

    error: {
        color: danger,
        paddingTop: 4,
        fontSize: 15,
    },
});