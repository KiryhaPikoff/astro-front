import {StyleSheet} from 'react-native';
import {danger} from '../../../assets/theme/Colors'

export default StyleSheet.create({
    wrapper: {
        height: 42,
        borderWidth: 2,
        borderRadius: 4,
        paddingHorizontal: 5,
        marginTop: 5,
    },

    icon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 6,
    },

    inputContainer: {
        paddingVertical: 8,
    },

    textInput: {
        flex: 1,
    },

    error: {
        color: danger,
        paddingTop: 4,
        fontSize: 15,
    },
});