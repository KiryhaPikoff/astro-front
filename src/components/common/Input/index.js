import React, {useState} from 'react';

import {Text, TextInput, View} from 'react-native';
import styles from './styles'
import Colors from '../../../assets/theme/Colors'
import {danger} from '../../../assets/theme/Colors'

export const ICON_POS_LEFT = 'left'
export const ICON_POS_RIGHT= 'right'

const Input = ({
   onChangeText,
   icon,
   iconPosition,
   style,
   value,
   label,
   error,
    enabled,
    keyboardType,
   ...props
}) => {

    const [focused, setFocused] = useState(false);

    const getFlexDirection = () => {
        if (icon && iconPosition) {
            if (iconPosition === ICON_POS_RIGHT) {
                return 'row-reverse';
            } else {
                return 'row';
            }
        }
    }

    const getBorderColor = () => {
        if (error) {
            return danger
        }
        if (focused) {
            return Colors.light.tabIconSelected
        } else {
            return Colors.light.tabIconDefault
        }
    }

    return (
        <View style={styles.inputContainer}>
            {label && <Text>{label}</Text>}

            <View style={[styles.wrapper, {borderColor: getBorderColor(), flexDirection: getFlexDirection()}]}>
                <View style={styles.icon}>{icon && icon}</View>

                <TextInput
                    editable={enabled !== null ? enabled : true}
                    selectTextOnFocus={enabled !== null ? enabled : true}
                    style={[styles.textInput,style]}
                    onChangeText={onChangeText}
                    value={value}
                    keyboardType={keyboardType ? keyboardType : "default"}
                    onFocus={() => {
                        setFocused(true)
                    }}
                    onBlur={() => {
                        setFocused(false)
                    }}
                    {...props}
                />
            </View>

            {error && <Text style={styles.error}>{error}</Text>}
        </View>
    );
};

export default Input;
