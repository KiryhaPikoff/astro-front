import React from 'react';
import {FlatList, Switch, Text, View} from 'react-native';
import {ListItem} from 'react-native-elements'
import {Ionicons} from "@expo/vector-icons";
import {ENABLED} from "../../../constants/actionsTypes";
import Colors from "../../../assets/theme/Colors";
import { StyleSheet } from "react-native";


export const UsersList = ({
    data,
    onAccountEnabledChange,
    onPasswordPolicyChange,
}) => {

    let keyExtractor = (item, _) => item.login

    let renderItem = ({item}) => (
        <ListItem bottomDivider containerStyle={{backgroundColor: Colors.light.tabIconDefault, marginTop: 10}}>
            <Ionicons name="person" size={42} color="blue" />
            <ListItem.Content>
                <ListItem.Title style={{fontWeight: 'bold'}}>{item.login}</ListItem.Title>
                <ListItem.Subtitle>{item.role}</ListItem.Subtitle>
            </ListItem.Content>

            <View style={styles.btns}>
                <View style={styles.btns}>
                    <Text>Enabled:</Text>
                    <Switch
                        value={item.userAccountMode === ENABLED}
                        onValueChange={() => onAccountEnabledChange(item.login)}
                    />
                </View>
                <View style={styles.btns}>
                    <Text>Password policy:</Text>
                    <Switch
                        value={item.passwordPolicy === ENABLED}
                        onValueChange={() => onPasswordPolicyChange(item.login)}
                    />
                </View>
            </View>
         </ListItem>
    )

    return (
        <FlatList
            contentContainerStyle={{
                paddingTop: "5%",
                paddingBottom: 40
            }}
            style={{paddingBottom: '3%'}}
            keyExtractor={keyExtractor}
            data={data}
            renderItem={renderItem}
        />
    )
};

export default UsersList;

const styles = StyleSheet.create({
    lineStyle:{
        width: 150,
        borderWidth: 2,
        borderColor:'black',
        margin:10,
    },
    btns: {
        flex: 1,
        flexDirection: 'column',
        alignContent : 'flex-start'
    }
});