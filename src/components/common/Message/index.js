import React, {useContext, useState} from 'react';

import {ActivityIndicator, Text, TextInput, TouchableOpacity, View} from 'react-native';
import styles from './styles'
import Colors from '../../../assets/theme/Colors'
import {danger as DANGER} from '../../../assets/theme/Colors'
import {loading as LOADING} from '../../../assets/theme/Colors'

export const ICON_POS_LEFT = 'left'
export const ICON_POS_RIGHT= 'right'

const Message = ({
    message,
    retry,
    retryFn,
    onDismiss,
    loading,
    primary,
    success,
    danger,
    info,
}) => {
    const [userDismissed, setDismissed] = useState()

    const getBackgroundColor = () => {
        if (primary) {
            return Colors.light.tabIconDefault
        }
        if (success) {
            return Colors.light.success
        }
        if (info) {
            return Colors.light.info
        }
        if (danger) {
            return DANGER
        }
    }

    return (
        <>
            {userDismissed ? null :
                <TouchableOpacity style={[styles.wrapper, {backgroundColor: getBackgroundColor()}]}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        {loading && <ActivityIndicator color={LOADING} />}
                        {!loading && message &&
                            <Text style={{color: Colors.light.text}}>{message}</Text>}

                        {retry &&
                            <TouchableOpacity onPress={retryFn}>
                                <Text>Retry</Text>
                            </TouchableOpacity>
                        }

                        {typeof onDismiss==="function" && (
                            <TouchableOpacity onPress={() => {
                            setDismissed(true)
                            onDismiss()
                        }}>
                            <Text>X</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                </TouchableOpacity>
                }
        </>
    );
};

export default Message;
