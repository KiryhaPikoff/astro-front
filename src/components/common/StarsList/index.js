import React from 'react';
import {FlatList} from 'react-native';
import Container from "../Container";
import {ListItem} from 'react-native-elements'
import {Ionicons} from "@expo/vector-icons";

export const StarsList = ({
    data,
    setSelectedStar
}) => {
    let starColorsMap = new Map();
    starColorsMap.set("O", "#1b19a6");
    starColorsMap.set("B", "#3734c9");
    starColorsMap.set("A", "#4a47f9");
    starColorsMap.set("F", "#52b2dd");
    starColorsMap.set("G", "#fbe462");
    starColorsMap.set("K", "#ff8656");
    starColorsMap.set("M", "#ff3d3d");

    const getIconColor = (spectralClass) => {
        let mapColor = starColorsMap.get(spectralClass)
        return mapColor ? mapColor : "#fcfcfc"
    }

    let keyExtractor = (item, index) => index.toString()

    let renderStar = ({item}) => (

        <ListItem bottomDivider>
            <Ionicons name="star" size={42} color={getIconColor(item.spectralClass)} />
            <ListItem.Content>
                <ListItem.Title style={{fontWeight: 'bold'}}>{item.name}</ListItem.Title>
                <ListItem.Subtitle>{item.starType}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron onPress={ () => {setSelectedStar(item)} } />
        </ListItem>
    )

    return (
        <Container>
            <FlatList
                keyExtractor={keyExtractor}
                data={data}
                renderItem={renderStar}
            />
        </Container>

    )
};

export default StarsList;
