import React, {useEffect, useState} from 'react';
import {Button, ScrollView, StyleSheet, Text} from 'react-native';
import Input from "../Input";
import Container from "../Container";
import CustomButton from "../CustomButton";
import {isAdmin} from "../../../helpers/roleHelper";
import {Divider} from "react-native-elements";

const LINE_WIDTH = 3

export const UpdateStar = ({
    star,
    onUpdate,
    onBack
}) => {
    const [form, setForm] = useState(star)
    const [errors, setErrors] = useState({})

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value })

        if (value !== '') {
            setErrors(prev=>{
                return {...prev, [name]: null}
            });
        } else {
            setErrors(prev=>{
                return {...prev, [name]: "This field is required"}
            });
        }
    }

    const onSubmit = () => {
        if(!form.name) {
            setErrors(prev=>{
                return {...prev, name: "Please add a name"}
            });
        }
        if (
            Object.values(form).length === 1 &&
            Object.values(form).every((item) => item.trim().length > 0) &&
            Object.values(errors).every((item) => !item)
        ) {
            onUpdate()
        }
    }

  return (
    <ScrollView>
        <Container>

            { isAdmin()
                ? <>
                    <CustomButton primary title={'Save'} onPress={onSubmit} />
                </>
                : <></>
            }
            <CustomButton primary title={'Back'} onPress={() => { onBack() }} />

            { isAdmin()
                ? <>
                    <Text style={styles.label}>Star id:</Text>
                    <Input
                        value={form.id}
                        enabled={false}
                        error={errors?.name}
                    />
                  </>
                : <></>
            }

            <Text style={styles.label}>Star name:</Text>
            { isAdmin()
                ? <Input
                    value={form.name}
                    onChangeText={(value) => {
                        onChange({name: 'name', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.name}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Absolute star surface temperature in Kelvins:</Text>
            { isAdmin()
                ?  <Input
                    value={form.absoluteTemperature.toString()}
                    onChangeText={(value) => {
                        onChange({name: 'absoluteTemperature', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.absoluteTemperature.toString()}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Relative luminosity calculated with respect to sun (L/Lo):</Text>
            { isAdmin()
                ?  <Input
                    value={form.relativeLuminosity.toString()}
                    onChangeText={(value) => {
                        onChange({name: 'relativeLuminosity', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.relativeLuminosity.toString()}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Relative radius calculated with respect to sun (R/Ro):</Text>
            { isAdmin()
                ?  <Input
                    value={form.relativeRadius.toString()}
                    onChangeText={(value) => {
                        onChange({name: 'relativeRadius', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.relativeRadius.toString()}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }


            <Text style={styles.label}>Absolute Visual magnitude (Mv):</Text>
            { isAdmin()
                ?  <Input
                    value={form.absoluteMagnitude.toString()}
                    onChangeText={(value) => {
                        onChange({name: 'absoluteMagnitude', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.absoluteMagnitude.toString()}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Star type:</Text>
            { isAdmin()
                ?  <Input
                    value={form.starType}
                    onChangeText={(value) => {
                        onChange({name: 'starType', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.starType}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Star spectral class:</Text>
            { isAdmin()
                ?  <Input
                    value={form.spectralClass}
                    onChangeText={(value) => {
                        onChange({name: 'spectralClass', value});
                    }}
                    error={errors?.name}
                />
                : <>
                    <Text style={styles.lvalue}>{form.spectralClass}</Text>
                    <Divider style={[styles.divider]} orientation="horizontal" width={LINE_WIDTH} />
                </>
            }

            <Text style={styles.label}>Star color after Spectral Analysis:</Text>
            { isAdmin()
                ?  <Input
                    value={form.starColor}
                    onChangeText={(value) => {
                        onChange({name: 'starColor', value});
                    }}
                    error={errors?.name}
                />
                : <Text style={styles.lvalue}>{form.starColor}</Text>
            }

        </Container>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '90%',
  },
  label: {
      marginTop: 10,
      marginBottom: -10,
      fontWeight: 'bold',
      fontSize: 16
  },
    lvalue: {
        marginTop: 10,
        fontSize: 20
    },
    divider: {
      marginTop: 10,
    }
});
