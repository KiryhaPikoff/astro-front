export const HOME_NAVIGATOR = "Home";

export const ABOUT = "About";
export const USERS = "Users";
export const STARS = "Stars";
export const SETTINGS = "Settings";

export const LOGIN = "Login";
export const REGISTER = "Register";