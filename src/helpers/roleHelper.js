import {useContext} from "react";
import {GlobalContext} from "../context/Provider";

export const ROLE_ADMIN = 'admin'
export const ROLE_USER = 'customer'
export const ANY = 'any'

export const isAdmin = () => {
    const {
        authState: {role},
    } = useContext(GlobalContext)

    return role === ROLE_ADMIN
}

export const isUser = () => {
    const {
        authState: {role},
    } = useContext(GlobalContext)

    return role === ROLE_USER
}

export const isInRoles = (roles) => {
    const {
        authState: {role},
    } = useContext(GlobalContext)

    return contains(ANY, roles) || contains(role, roles)
}

export const username = () => {
    const {
        authState: {username},
    } = useContext(GlobalContext)

    return username
}

export const jwt = () => {
    const {
        authState: {data},
    } = useContext(GlobalContext)

    return data.access
}

export const contains = (str, arr) => {
    return arr.filter((strs) => str == strs).length > 0
}


export const isAuthenticated = () => {
    const {
        authState: {isLoggedIn},
    } = useContext(GlobalContext)

    return isLoggedIn
}
