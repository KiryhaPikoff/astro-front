import axios from "axios";

const axiosInstance = axios.create({
    withCredentials: false,
    baseURL: 'http://localhost:3064',
    headers: {
        'Access-Control-Allow-Headers': 'Authorization, Content-Type, Range',
    },
});

export default axiosInstance;


