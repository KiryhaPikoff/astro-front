import axios from "axios";
import envs from "../config/env"
import AsyncStorage from '@react-native-async-storage/async-storage';

let headers = {
    'Access-Control-Allow-Headers': 'Authorization, Content-Type, Range',
};

const axiosInstance = axios.create({
    baseURL: envs.BACKEND_URL,
    headers,
});

axiosInstance.interceptors.request.use(
    async (config) => {
        console.log(config)
        const token = await AsyncStorage.getItem('token')
        if (token !== null && token !== '' && config.url !== "/auth/v1/tokens" && config.url !== "/auth/v1/register") {
            config.headers.Authorization = `Bearer ${token}`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)

export default axiosInstance;


