import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {useTheme} from 'react-native-paper';
import DrawerNavigator from "./DrawerNavigator";
import AuthNavigator from "./AuthNavigator";
import {isAuthenticated} from "../helpers/roleHelper";

export default function Navigation() {
    const theme = useTheme();

    return (
        <NavigationContainer theme={theme}>
            { isAuthenticated() ? <DrawerNavigator /> : <AuthNavigator /> }
        </NavigationContainer>
    );
}
