import React from 'react';

import {Alert, Image, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import Container from "../../components/common/Container";
import styles from "./styles";
import {useTheme} from "react-native-paper";
import {ABOUT, SETTINGS, STARS, USERS} from "../../constants/RouteNames";
import {logout} from "../../context/actions/auth/logout";
import {Ionicons} from "@expo/vector-icons";
import {ListItem} from "react-native-elements";
import {ANY, isInRoles, ROLE_ADMIN, username} from "../../helpers/roleHelper";

const SideMenu = ({navigation, authDispatch}) => {
    let theme = useTheme()

    const handleLogout = () => {
        Alert.alert("Logout!", "Are you sure you want to logout?", [
            {
                text: "Cancel",
                onPress: () => {}
            },
            {
                text: "OK",
                onPress: () => {
                    logout()(authDispatch)
                }
            }
        ])
    }

    const menuItems = [
        {
            icon: <Ionicons name="happy" size={30} color="green" />,
            name: "Hi, " + username() + "!",
            roles: [ANY],
            onPress: () => {},
        },
        {
            icon: <Ionicons name="people-circle" size={30} color="blue" />,
            name: "Users",
            roles: [ROLE_ADMIN],
            onPress: () => {
                navigation.navigate(USERS)
            },
        },
        {
            icon: <Ionicons name="star" size={30} color="orange" />,
            name: "Stars",
            roles: [ANY],
            onPress: () => {
                navigation.navigate(STARS)
            },
        },
        {
            icon: <Ionicons name="settings" size={30} color="black"/>,
            name: "Settings",
            roles: [ANY],
            onPress: () => {
                navigation.navigate(SETTINGS)
            },
        },
        {
            icon: <Ionicons name="code" size={30} color="black"/>,
            name: "About",
            roles: [ANY],
            onPress: () => {
                navigation.navigate(ABOUT)
            },
        },
        {
            icon: <Ionicons name="exit" size={30} color="black" />,
            name: "Logout",
            roles: [ANY],
            onPress: handleLogout
        },
    ]

    return (
        <SafeAreaView style={{backgroundColor: 'red'}}>
            <Container>
                <Image
                    source={require('../../assets/images/logo.png')}
                    style={styles.logoImage}
                />

                <View style={{paddingHorizontal: 65}}>
                    {menuItems
                        .filter(({roles}) => isInRoles(roles))
                        .map(({name, icon, onPress}) =>
                        <TouchableOpacity onPress={onPress} key={name} style={styles.item}>
                            {icon}
                            <Text style={[styles.itemText, {color: theme.colors.text}]}>{name}</Text>
                        </TouchableOpacity>
                    )}
                </View>
            </Container>
        </SafeAreaView>
    );
};

export default SideMenu;
