import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    logoImage: {
        height: 70,
        width: 70,
        alignSelf:'center',
        marginTop: 10,
        marginBottom: 10,
    },

    item: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    itemText: {
        fontSize: 17,
        paddingVertical: 7,
        paddingLeft: 16
    }
});