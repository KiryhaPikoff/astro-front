import * as React from "react";
import {createDrawerNavigator} from "@react-navigation/drawer";
import HomeNavigator from "./HomeNavigator";
import {HOME_NAVIGATOR} from "../constants/RouteNames";
import SideMenu from "./SideMenu";
import {useContext} from "react";
import {GlobalContext} from "../context/Provider";

const getDrawerContent = (navigation, authDispatch) => {
    return <SideMenu navigation={navigation} authDispatch={authDispatch} />
}

export default function DrawerNavigator() {
    const Drawer = createDrawerNavigator();
    const {authDispatch} = useContext(GlobalContext)

    return (
        <Drawer.Navigator drawerType={'back'}
                          drawerContent={
                              ({navigation}) => getDrawerContent(navigation, authDispatch)
                          }
                          screenOptions={{ headerShown: false }}>
            <Drawer.Screen name={HOME_NAVIGATOR} component={HomeNavigator} />
        </Drawer.Navigator>
    );
}