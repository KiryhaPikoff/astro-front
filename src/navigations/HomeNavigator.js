import {createNativeStackNavigator} from "@react-navigation/native-stack";
import * as React from "react";
import {ABOUT, SETTINGS, STARS, USERS} from "../constants/RouteNames";
import {About} from "../screens/About"
import {Users} from "../screens/Users"
import {Stars} from "../screens/Stars";
import {Settings} from "../screens/Settings";

export default function HomeNavigator() {
    const HomeStack = createNativeStackNavigator();

    return (
        <HomeStack.Navigator initialRouteName={ABOUT} screenOptions={{ headerTitleAlign: 'center'}} >
            <HomeStack.Screen name={USERS} component={Users} />
            <HomeStack.Screen name={STARS} component={Stars} />
            <HomeStack.Screen name={SETTINGS} component={Settings} />
            <HomeStack.Screen name={ABOUT} component={About} />
        </HomeStack.Navigator>
    );
}