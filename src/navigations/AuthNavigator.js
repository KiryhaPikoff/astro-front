import {createNativeStackNavigator} from "@react-navigation/native-stack";
import * as React from "react";
import {LOGIN, REGISTER} from "../constants/RouteNames";
import {Login} from "../screens/Login";
import {Register} from "../screens/Register";


export default function AuthNavigator() {
    const AuthStack = createNativeStackNavigator();

    return (
        <AuthStack.Navigator screenOptions={{headerShown: false}}>
            <AuthStack.Screen name={LOGIN} component={Login} />
            <AuthStack.Screen name={REGISTER} component={Register} />
        </AuthStack.Navigator>
    );
}